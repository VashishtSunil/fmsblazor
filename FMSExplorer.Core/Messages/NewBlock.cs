﻿using System.Numerics;

namespace FMSExplorer.Messages
{
    public class NewBlock
    {
        public NewBlock(BigInteger blockNumber)
        {
            BlockNumber = blockNumber;
        }

        public BigInteger BlockNumber { get; }
    }
}
